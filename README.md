# Neovim config 
My personal neovim config

## Table of contents
- [Dependencies](#dependencies)
- [Installation](#installation)
- [Keymaps](#keymaps)
- [environment-setup](#environment-setup)


## Dependencies 
### Fedora
```bash
sudo dnf install neovim git gcc ripgrep 
```
### Arch
```bash
sudo pacman -S neovim git 
```

## Installation 

### Setup icons
- Download a patched font from [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts/releases)
- Reload Fonts `fc-cache -f -v`
- Set as your terminal font


### Setup nvim
- HTTP
```bash
git clone https://gitlab.com/kesley/neovim-config.git  ~/.config/nvim 
```

- SSH
```bash
git clone git@gitlab.com:kesley/neovim-config.git  ~/.config/nvim 
```

## Keymaps
### Structure
```lua
MODE KEYMAP  -- DESCRIPTION
```

#### Customs
```lua
-- Window
n <Space>sv  -- Vertical Split
n <Space>sx  -- Close

-- Cursor
n <C-h>      -- Move to LEFT window
n <C-j>      -- Move to BOTTOM window
n <C-k>      -- Move to TOP window
n <C-l>      -- Move to RIGHT window

-- Buffers
n <S-h>      -- Move to previous
n <S-l>      -- Move to next 
n <Space>bd  -- delete

-- Exit modes
i kj         -- Insert
i lk         -- Visual

-- Clear search highlight
n <Space>nh

-- Redo
n U            
```

#### Plugins
```lua
-- Telescope
n <Space><Space>    -- Open find files
n <Space>/          -- Open live grep
n <Space>fb         -- Open buffers
n <Space>fh         -- Open helper tags

-- Oil
n -            -- Open

-- Aerial
n <Space>a     -- Toggle
n {            -- Previous
n }            -- Next

-- Neogit
n <Space>ng    -- Open

-- LSP
n K            -- Info
n [d           -- Previous diagnostic
n ]d           -- Next diagnostic
n <Space>d     -- Open line diagnostic
n <Space>rn    -- Rename
n <Space>rs    -- Restart LSP
n/v <Space>ca  -- Code Actions

-- Nvim Completation
n <C-e>        -- Abort completation
n CR           -- Confirm 
```

## Environment Setup
### Rust
```lua
:TSInstall rust
:MasonInstall rust-analyzer
```

### Vue
```lua
:TSInstall vue
:MasonInstall vue-language-server
```

### JavaScript / Typescript
```lua
:TSInstall javaScript typescript
:MasonInstall typescript-language-server
```

### Svelte
```lua
:TSInstall svelte
:MasonInstall svelte-language-server
```

### YAML
```lua
:TSInstall yaml
```

### Lua
```lua
:TSInstall lua luadoc
:MasonInstall lua-language-server stylua
```

### Markdown
```lua
:TSInstall markdown 
:MasonInstall marksman
```

### Ruby
```lua
:TSInstall ruby
:MasonInstall solargraph
```

### PHP
```lua
:TSInstall php phpdoc 
:MasonInstall phpactor pretty-php
```

### CSS / SASS / LESS
```lua
:TSInstall css scss
:MasonInstall css-lsp cssmodules-language-server 
```

### HTML
```lua
:TSInstall html
:MasonInstall emmet-language-server
```

### Twig
```lua
:TSInstall twig
:MasonInstall twiggy-language-server
```

### XML
```lua
:TSInstall xml
```

### Go
```lua
:TSInstall go gomod gosum
:MasonInstall gopls 
```
