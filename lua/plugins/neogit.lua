local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

return {
  "NeogitOrg/neogit",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-telescope/telescope.nvim",
    "sindrets/diffview.nvim",
  },
  config = function()
    require("neogit").setup()

    keymap("n", "<Space>gg", ":Neogit<CR>", opts)
  end,
}
