return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    require("telescope").setup()

    local builtin = require("telescope.builtin")

    vim.keymap.set("n", "<Space><Space>", builtin.find_files, {})
    vim.keymap.set("n", "<Space>/", builtin.live_grep, {})
    vim.keymap.set("n", "<Space>fb", builtin.buffers, {})
    vim.keymap.set("n", "<Space>fh", builtin.help_tags, {})
  end,
}
