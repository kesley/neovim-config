local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

return {
  "nvim-tree/nvim-tree.lua",
  version = "*",
  lazy = false,
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
  config = function()
    require("nvim-tree").setup({
      sort = {
        sorter = "case_sensitive",
      },
      view = {
        number = false,
        relativenumber = true,
        float = {
          enable = true,
          open_win_config = {
            width = 50,
          },
        },
      },
      filters = {
        dotfiles = true,
      },
    })

    keymap("n", "<Space>nn", ":NvimTreeToggle<CR>", opts)
    keymap("n", "<Space>nf", ":NvimTreeFindFile<CR>", opts)
    keymap("n", "<Space>nr", ":NvimTreeRefresh<CR>", opts)
    keymap("n", "<Space>nc", ":NvimTreeCollapse<CR>", opts)
  end,
}
