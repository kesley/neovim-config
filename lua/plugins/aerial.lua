local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

return {
  "stevearc/aerial.nvim",
  dependencies = {
    "nvim-treesitter/nvim-treesitter",
    "nvim-tree/nvim-web-devicons",
  },
  opts = {
    on_attach = function(bufnr)
      keymap("n", "{", ":AerialPrev<CR>", { buffer = bufnr })
      keymap("n", "}", ":AerialNext<CR>", { buffer = bufnr })
      keymap("n", "<Space>a", ":AerialToggle!<CR>", opts)
    end,
  },
}
