local M = {}

M.daps_setup = function()
  -- DAP Setup
end

M.keymaps_setup = function()
  -- DAP UI
  vim.keymap.set("n", "<Space>du", function()
    M.dapui.toggle()
  end)

  local bkc = function()
    M.dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
  end

  local bkl = function()
    M.dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
  end

  -- DAP
  vim.keymap.set("n", "<F5>", M.dap.continue)
  vim.keymap.set("n", "<F10>", M.dap.step_over)
  vim.keymap.set("n", "<F11>", M.dap.step_into)
  vim.keymap.set("n", "<F12>", M.dap.step_out)
  vim.keymap.set("n", "<Space>db", M.dap.toggle_breakpoint)
  vim.keymap.set("n", "<Space>dB", bkc)
  vim.keymap.set("n", "<Space>lp", bkl)
  vim.keymap.set("n", "<Space>dr", M.dap.repl.open)
  vim.keymap.set("n", "<Space>dl", M.dap.run_last)
  vim.keymap.set("n", "<Space>dk", M.dap.continue)
end

return {
  "mfussenegger/nvim-dap",
  dependencies = {
    "rcarriga/nvim-dap-ui",
    "nvim-neotest/nvim-nio",
  },
  config = function()
    M.dap = require("dap")
    M.dapui = require("dapui")

    M.dapui.setup()

    M.keymaps_setup()
    M.daps_setup()
  end,
}
